import { 
  Routes, 
  BrowserRouter as Router, 
  Route,
  Link
} from 'react-router-dom'

import Clock from './pages/clocks/Clock'
import OptimizedClock from './pages/clocks/OptimizedClock'
import Memory from './pages/memory-game/Memory'
import Todos from './pages/todo-app/Todos'
import Home from './pages/home/Home'
import Bingo from './pages/bingo/Bingo'
import './app.css'
import homeIcon from './assets/home-icon.png'
import Typicode from './pages/typicode/Typicode'
import NotFound from './pages/page-not-found/NotFound'
import Login from './pages/login/Login'
import Profile from './pages/profile/Profile'
import Map from './pages/map/Map'
import Language from './pages/language/Language'
import { ContextProvider } from './Context'

const App = () => {
  return (
    <div className='main'>
      <ContextProvider>
        <Router>
          <nav>
            <Link className='nav-home' to='/'>
              <img 
                src={homeIcon}
                alt='Home-icon'
              ></img>
            </Link>
            <Link className='nav-link' to='/clock'>Not-So-Good-Clock</Link>
            <Link className='nav-link' to='/optimizedclock'>Better Clock</Link>
            <Link className='nav-link' to='/memory'>Memory Game</Link>
            <Link className='nav-link' to='/todos'>Todo Application</Link>
            <Link className='nav-link' to='/bingo'>Bingo</Link>
            <Link className='nav-link' to='/typicode'>Typicode</Link>
          </nav>
          <Routes>
            <Route path='/' element={<Home/>}></Route>
            <Route path='/clock' element={<Clock/>}></Route>
            <Route path='/optimizedclock' element={<OptimizedClock/>}></Route>
            <Route path='/memory' element={<Memory/>}></Route>
            <Route path='/todos' element={<Todos/>}></Route>
            <Route path='/bingo' element={<Bingo/>}>
              <Route path='numerot' element={<Bingo/>}></Route>
            </Route>
            <Route path='/typicode' element={<Typicode/>}></Route>
            <Route path='/login' element={<Login/>}></Route>
            <Route path='/profile/:id' element={<Profile/>}></Route>
            <Route path='/map' element={<Map/>}></Route>
            <Route path='/language' element={<Language/>}></Route>
            <Route path='*' element={<NotFound/>}></Route> 
          </Routes>
          <footer></footer>
        </Router>
      </ContextProvider>
    </div>
  )
}

export default App;