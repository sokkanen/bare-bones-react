import { useState, createContext } from 'react'

export const AppContext = createContext([{}, () => {}]);

export const ContextProvider = (props) => {
    const initialContext = {
        language: 'finnish',
        user: {
            name: '',
            address: '',
            major: '',
            nationality: ''
        }
    }
    const [state, setState] = useState(initialContext)
    return (
        <AppContext.Provider value={[state, setState]}>
            {props.children}
        </AppContext.Provider>
    )
}