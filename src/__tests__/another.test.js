import { render, unmountComponentAtNode } from "react-dom";
import {cleanup, fireEvent, screen, prettyDOM} from '@testing-library/react';
import { useState } from 'react'

// Could be imported from somewhere...
const ComponentWithButton = () => {
  const [color, setColor] = useState('punainen')
  const handleClick = () => {
    setColor('sininen')
  }
  return (
    <div>
      <button id="nappula" onClick={handleClick}>change color</button>
      <p id="teksti">Hevonen on {color}.</p>
    </div>
  )
}

let container = null;
beforeEach(() => {
  container = document.createElement("div")
  document.body.appendChild(container)
})

afterEach(() => {
  unmountComponentAtNode(container)
  container.remove()
  container = null
  cleanup()
})

describe("Testing ComponentWithButton", () => {
  it('clicking button changes the color', () => {
    render(<ComponentWithButton/>, container)
    const element = screen.getByText("Hevonen on punainen.")
    expect(element.textContent).toBe("Hevonen on punainen.")
    expect(element.id).toBe("teksti")
    // Debugging
    console.log(prettyDOM(container))

    fireEvent.click(screen.getByText('change color'));
    const changedElement = screen.getByText("Hevonen on sininen.")
    // Debugging
    console.log(prettyDOM(container))
    expect(changedElement.textContent).toBe("Hevonen on sininen.")
    expect(changedElement.id).toBe("teksti")
  })
})