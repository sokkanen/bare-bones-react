import { render, unmountComponentAtNode } from "react-dom";
import {cleanup, fireEvent, render as testRender} from '@testing-library/react';
// Helps in debugging!
import { prettyDOM } from "@testing-library/react";

// Could be imported from somewhere...
const Hello = ({ name }) => {
    return (
        <p>{`Hello ${name}!`}</p>
    )
}

let container = null;
beforeEach(() => {
  container = document.createElement("div")
  document.body.appendChild(container)
})

afterEach(() => {
  unmountComponentAtNode(container)
  container.remove()
  container = null
  cleanup()
})

describe("renders with a name", () => {
  it('returns correct message', () => {
    render(<Hello name={'Keijo'}/>, container);
    expect(container.textContent).toBe("Hello Keijo!")
  })
  it('returns correct message with another props', () => {
    render(<Hello name={'Maija'}/>, container);
    expect(container.textContent).toBe("Hello Maija!")
  })
})