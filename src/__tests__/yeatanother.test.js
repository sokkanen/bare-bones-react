/* eslint-disable testing-library/no-node-access */
import { render, unmountComponentAtNode } from "react-dom";
import {cleanup, fireEvent, screen, prettyDOM } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import { useState } from "react";

const ComponentWithForm = ({ formSubmit }) => {
    const [user, setUser] = useState('nobody')

    return (
        <div>
            <p id="user">Logged in: {user}</p>
            <form onSubmit={(e) => formSubmit(e, user)}>
                <label htmlFor="username">Name</label>
                <input 
                type="text" 
                id="username" 
                placeholder="enter your name"
                onChange={e => setUser(e.target.value)}
                >
                </input>
                <button type="submit">Login</button>
            </form>
        </div>
    )
}

let container = null;
beforeEach(() => {
  container = document.createElement("div")
  document.body.appendChild(container)
})

afterEach(() => {
  unmountComponentAtNode(container)
  container.remove()
  container = null
  cleanup()
})

describe("Testing ComponentWithForm", () => {
  it('User input works', () => {
    const submitFunction = jest.fn((e, user) => e.preventDefault());
    render(<ComponentWithForm formSubmit={submitFunction}/>, container)

    const element = screen.getByText("Logged in: nobody")
    expect(element.textContent).toBe("Logged in: nobody")
    expect(element.id).toBe("user")
    const input = screen.getByPlaceholderText('enter your name')
    const saveButton = screen.getByText("Login")

    userEvent.type(input, 'Joel')
    userEvent.click(saveButton)

    const changedElement = screen.getByText("Logged in: Joel")
    expect(changedElement.textContent).toBe("Logged in: Joel")
    expect(changedElement.id).toBe("user")
    expect(submitFunction.mock.calls).toHaveLength(1)
    expect(submitFunction.mock.calls[0][1]).toBe('Joel' )
  })
})