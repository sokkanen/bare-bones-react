import { useState } from "react"
import './bingo.scss'

const Bingo = () => {
    const generateRandomNumber = (numbers) => {
        const number = Math.floor(Math.random() * 75 + 1)
        if (numbers.includes(number)) {
            return generateRandomNumber(numbers)
        } else {
            return number
        }
    }
    const drawNumber = () => {
        if (numbers.length === 75) {
            alert('You have drawn all the possible bingo numbers.')
        } else {
            const newNumber = generateRandomNumber(numbers)
            setNumbers((numbers) => {
                return [...numbers, newNumber]
            })
        }
    }

    const reset = () => {
        setNumbers([])
    }

    const [numbers, setNumbers] = useState([])
    return (
        <div className="bingo-main">
            <button onClick={drawNumber}>Draw a new number</button>
            <button onClick={reset}>Reset</button>
            {numbers.length !== 0 && 
            <ul>
                {numbers.map((n) => {
                    return <li key={`k_${n}`}>{n}</li>
                })}
            </ul>
            }
        </div>
    )
}

export default Bingo