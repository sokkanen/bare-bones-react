import { useState } from "react"
import './clock.css'

// LEAKS MEMORY! NOT TO BE USED IF THE CLOCK IS NOT RENDERED AT ALL TIMES!
const Clock = () => {
    const [hours, setHours] = useState('00')
    const [minutes, setMinutes] = useState('00')
    const [seconds, setSeconds] = useState('00')
  
    const setTime = (time, setter) => {
      setter(time < 10 ? '0' + time : time)
    }
    
    setInterval(() => {
      const now = new Date()
      now.toLocaleTimeString()
      setTime(now.getSeconds(), setSeconds)
      setTime(now.getMinutes(), setMinutes)
      setTime(now.getHours(), setHours)
    }, 1000)
  
    return (
      <div className='kello'>
        <h1>{hours}:{minutes}:{seconds}</h1>
      </div>
    )
}

export default Clock