import { useState, useEffect } from "react"
import './clock.css'

// Does not leak memory
// useEffect's return function is used to clear the timeout.
const Clock = () => {
  const locale = 'fi-FI' 
  const [timeString, setTimeString] = useState('00:00:00')
  useEffect(() => {
    const id = setInterval(() => {
      const now = new Date()
      const timeString = now.toLocaleTimeString(locale).replaceAll('.', ':')
      setTimeString(timeString)
    }, 1000)
    return () => clearInterval(id)
  })
    return (
      <div className='kello'>
        <h1>{timeString}</h1>
      </div>
    )
}

export default Clock