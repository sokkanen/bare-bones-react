import { useContext } from 'react'
import { AppContext } from '../../Context'
import './language.css'

const Language = () => {
    const [state, setState] = useContext(AppContext)

    const texts = {
        finnish: {
            button: 'Vaihda kieli',
            name: 'Nimi',
            address: 'Osoite',
            nationality: 'Kansalaisuus',
            major: 'Pääaine',
            save: 'Tallenna'
        },
        english: {
            button: 'Change language',
            name: 'Name',
            address: 'Address',
            nationality: 'Nationality',
            major: 'Major',
            save: 'Save'
        }
    }

    const changeLanguage = () => {
        setState((state) => { 
            const newLanguage = state.language === 'finnish' ? 'english' : 'finnish'
            return { ...state, language: newLanguage }
        })
    }

    const setInfo = (event) => {
        event.preventDefault()
        const user = {
            name: event.target.name.value,
            nationality: event.target.nationality.value,
            address: event.target.address.value,
            major: event.target.major.value,
        }
        event.target.name.value = ''
        event.target.nationality.value = ''
        event.target.address.value = ''
        event.target.major.value = ''
        setState((state) => { 
            return { ...state, user: user}
        })
    }

    return (
        <div className='language-main'>
            <form onSubmit={setInfo} className='lomake'>
                <label htmlFor='name'>{texts[state.language].name}</label>
                <input id="name" type="text"></input>
                <label htmlFor='nationality'>{texts[state.language].nationality}</label>
                <input id="nationality" type="text"></input>
                <label htmlFor='address'>{texts[state.language].address}</label>
                <input id="address" type="text"></input>
                <label htmlFor='major'>{texts[state.language].major}</label>
                <input id="major" type="text"></input>
                <button className="nappula" type='submit'>{texts[state.language].save}</button>
            </form>
            <button className="nappula" onClick={changeLanguage}>
                {texts[state.language].button}
            </button>
            <table>
                <thead>
                    <tr>
                        <th>{texts[state.language].name}</th>
                        <th>{texts[state.language].nationality}</th>
                        <th>{texts[state.language].address}</th>
                        <th>{texts[state.language].major}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{state.user.name}</td>
                        <td>{state.user.nationality}</td>
                        <td>{state.user.address}</td>
                        <td>{state.user.major}</td>   
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default Language