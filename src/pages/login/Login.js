import { useState } from 'react';
import { useNavigate } from 'react-router-dom'

const LoginForm = () => {
    const navigate = useNavigate()
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const handleOnSubmit = e => {
        e.preventDefault();
        // Jonkinlaista kirjaumislogiikkaa
        navigate('/')
    };

    return (
        <form style={{position: 'absolute', top: '200px'}} onSubmit={handleOnSubmit}>
            <input type="text" value={username} onChange={e => { setUsername(e.target.value); }} />
            <input type="password" value={password} onChange={e => { setPassword(e.target.value); }} />
            <input type="submit" value="Login" />
        </form>
    )
}

export default LoginForm
