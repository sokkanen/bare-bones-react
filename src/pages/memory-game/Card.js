import { useState } from "react"
import './card.css'

const coverImgUrl = "https://i.imgur.com/zPwGJAZ.jpeg"

const Card = ({ card, revealCard }) => {
  const [hovering, setHovering] = useState(false)

  return (
    <div
      onMouseEnter={() => setHovering(true)}
      onMouseLeave={() => setHovering(false)}
      onClick={card.revealed ? null : () => revealCard(card)}
      className={card.revealed ? "card revealed" : hovering ? "card hovering" : "card"}
    >
      <img
        src={card.revealed ? card.img : coverImgUrl}
        alt={card.revealed ? "Front image" : "Back image"}
      ></img>
    </div>
  )
}

export default Card