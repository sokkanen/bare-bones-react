import { useState, useEffect } from "react"
import Card from "./Card"
import './memory.css'

import book_img from "./images/book.png"
import cards_img from "./images/cards.png"
import cd_img from "./images/cd.png"
import skull_img from "./images/skull.png"
import star_img from "./images/star.png"
import troll_img from "./images/troll.png"

const gridHeight = 3
const gridWidth = 4

const shuffle = (array) => {
  return array.forEach((_elem, i) => {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  })
}

const Memory = () => {
  const [firstCard, setFirstCard] = useState(null)
  const [deck, setDeck] = useState([])
  const [revealTimeout, setRevealTimeout] = useState(false)
  const [tries, setTries] = useState(0)

  useEffect(() => {
    const initializeGame = () => {
      if (window.confirm(`You used ${tries} tries. Play again?`)) {
        initializeGrid()
        setTries(0)
      }
    }
    const anyCardHidden = deck.length !== 0 && deck.find((card) => !card.revealed)
    let id
    if (!anyCardHidden && tries !== 0) {
      id = setTimeout(() => {
        initializeGame()
      }, 1200)
    }
    return () => clearTimeout(id)
  }, [deck, tries])

  const initializeGrid = () => {
    let images = [
      book_img,
      cards_img,
      cd_img,
      skull_img,
      star_img,
      troll_img,
    ];

    shuffle(images)

    let tempGrid = []
    let iter = 0;
    for (let i = 0; i < gridHeight; i++) {
      for (let j = 0; j < gridWidth; j++) {
        if (iter === images.length) {
          // After each image had been set once, set them all again.
          shuffle(images)
          iter = 0;
        }
        tempGrid.push({
          row: i,
          col: j,
          selected: false,
          revealed: false,
          img: images[iter],
        });
        iter++
      }
    }

    setDeck(tempGrid)
  };

  const revealCard = (card) => {
    const modifyRevealStatus = (revealed, selected) =>
      deck.map((c) =>
        c.selected || c === card
          ? {
              ...c,
              revealed,
              selected,
            }
          : { ...c }
      )

    const pairFound = () => {
      const newDeck = modifyRevealStatus(true, false)
      setDeck(newDeck)
      setRevealTimeout(false)
    }

    const pairNotFound = () => {
      setTimeout(() => {
        const newDeck = modifyRevealStatus(false, false)
        setDeck(newDeck)
        setRevealTimeout(false)
      }, 1000)
    }

    // If we are showing two non-matching cards already, do nothing
    if (revealTimeout) {
      return
    }
    // reveal the clicked card
    const newDeck = modifyRevealStatus(true, true)
    setDeck(newDeck)

    // Have we already revealed one card?
    if (firstCard) {
      // Make sure we didn't click on the same card twice
      if (card === firstCard) {
        return
      }

      setRevealTimeout(true)
      // Create a new deck to show the second clicked card as well
      // Check if a match is found
      setTries(tries + 1)
      card.img === firstCard.img ?
        pairFound() :
        pairNotFound()
      setFirstCard(null)
    } else {
      setFirstCard(card)
    }
  }

  if (deck.length === 0) {
    initializeGrid()
  }

  const renderDeck = () =>
    deck.map((card, i) => (
      <Card key={`${card.img}${i}`} card={card} revealCard={revealCard} />
    ));

  return (
    <div className="memory-main">
      {renderDeck()}
    </div>
  )
}

export default Memory