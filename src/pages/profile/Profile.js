import { useParams } from 'react-router-dom'

const Profile = () => {
    const params = useParams()
    const id = params.id
    return (
        <>
            <h1 className='title'>Profile page for user id {id}</h1>
        </>
    )
}

export default Profile