import { useState } from 'react'
import './todos.css'

const createRandomId = (idString) => {
    const id = Math.floor(Math.random() * 999999 + 1)
    return `${idString}_${id}`
}

const TodoForm = ({ addTodo }) => {
    return (
        <form className='todos-form' onSubmit={addTodo}>
            <label htmlFor='task'>Enter a new task</label>
            <input type='text' id='task'/>
            <input type='submit'/>
        </form>
    )
}

const TodoTable = ({ todos, removeTodo, setAsDone }) => {

    const handleSetAsDone = (event, id) => {
        event.preventDefault()
        setAsDone(id)
    }

    const handleRemove = (event, id) => {
        event.preventDefault()
        removeTodo(id)
    }

    const todosJsx = todos.map(todo => 
        <tr key={todo.id}>
            <td>{todo.task}</td>
            <td 
            className={todo.done ? 'clickable-td done' : 'clickable-td undone'}
            onClick={e => handleSetAsDone(e, todo.id)}>{todo.done ? 'yes' : 'no'}</td>
            <td><button onClick={e => handleRemove(e, todo.id)}>Remove</button></td>
        </tr>
    )
    return todosJsx.length !== 0 && (
        <table>
            <thead>
                <tr>
                    <th>Task</th>
                    <th>Done</th>
                    <th>Remove</th>
                </tr>
            </thead>
            <tbody>
                {todosJsx}
            </tbody>
        </table>
    )

}

const Todos = () => {
    const [todos, setTodos] = useState([])

    const addTodo = (event) => {
        event.preventDefault()
        const task = event.target.task.value
        event.target.task.value = ''
        const todo = {
            id: createRandomId(task),
            task,
            done: false
        }
        setTodos(todos => {
            return [todo, ...todos]
        })
    }

    const removeTodo = (id) => {
        const remainingTodos = todos.filter(t => t.id !== id)
        setTodos(remainingTodos)
    }

    const setAsDone = (id) => {
        const todo = todos.find(t => t.id === id)
        todo.done = !todo.done
        const updatedTodos = todos.map(t => t.id === id ? todo : t)
        setTodos(updatedTodos)
    }

    return (
        <div className='todos-main'>
            <TodoTable todos={todos} removeTodo={removeTodo} setAsDone={setAsDone}/>
            <TodoForm addTodo={addTodo}/>
        </div>
    )
}

export default Todos