import { useState, useEffect } from 'react'
import { getAllTodos } from '../../services/todoService'
import { useNavigate, useNavigation } from 'react-router-dom'
import './typicode.css'

const Typicode = () => {
    const [categories, setTodos] = useState([])
    const navigate = useNavigate()
    useEffect(() => {
        const getTodos = async () => {
            const data = await getAllTodos()
            setTodos(data)
        }
        getTodos()
    }, [])

    const handleSelect = (event) => {
        event.preventDefault()
        const path = event.target.value
        const cleanPath = path.replace('\'', '').replace(' ', '')
        navigate(cleanPath)
    }

    return (
        <div className='typicode-main'>
            <h1>Categories</h1>
            <select onChange={e => handleSelect(e)} name="categories" id="categories">
                {categories.map((category) => {
                    return <option 
                        className='typecode-li'
                        value={category}
                        key={category}
                        id={category}
                    >{category}</option>
                })}
            </select>
        </div>
    )
}

export default Typicode